// @flow
import React from 'react';
import PropTypes from 'prop-types';
import QBApi from './QBApi';

class QuickAuth extends React.Component {
    constructor(props){
        super(props);

        this.state = {ticket: ""};

        this.QBApi = new QBApi();
    }

    async componentDidMount(): void {
        const ticket = await this.QBApi.auth();

        this.setState({ticket});
    }

    render() {
        const {ticket} = this.state;

        return(<div>
            <p>error: {ticket.errcode}</p>
            <p>error text: {ticket.errtext}</p>
            <p>data: {ticket.data}</p>
        </div>);
    }
}

QuickAuth.propTypes = {
};

QuickAuth.defaultProps = {
};

export default QuickAuth;