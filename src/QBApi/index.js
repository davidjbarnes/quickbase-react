// @flow
import axios from 'axios';
const convert = require('xml-js');

class Index {
    constructor(domain: String, username: String, password: String) {
        this.domain = domain || "quickbase.com";
        this.username = username || "jeffy";
        this.password = password || "abc123";
    }

    async auth(): String {
        const url = "https://".concat(this.domain,
            "/db/main?a=API_Authenticate&username=", this.username,
            "&password=", this.password,
            "&hours=24");
        const response = await axios.get(url);

        if(response.status === 200) {
            const responseJson = JSON.parse(convert.xml2json(response.data, {ignoreDeclaration: true, compact: true, spaces: 4})).qdbapi;

            console.log(responseJson);
            return {
                errcode:responseJson.errcode._text,
                errtext: responseJson.errtext._text,
                data: responseJson.ticket ? responseJson.ticket._text : null
            };
        } else {
            return undefined;
        }
    }

}

export default Index;