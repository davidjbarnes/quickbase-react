# QuickBase-React
QuickBase API with React Implementation

## Getting Started
Install from npm:
```
$ npm i quickbase-react
```
Or get the source code here:
```
$ git remote add origin https://davidjbarnes@bitbucket.org/davidjbarnes/quickbase-react.git
```

## Usage
```
import QuickAuth from 'quickbase-react';
```

## Props


## Contributing
Email me: NullableInt@gmail.com 

## Authors

* **David J Barnes** - *NullableInt@gmail.com*

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details